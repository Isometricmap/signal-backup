import backup_pb2
from Crypto.Hash import HMAC, SHA256, SHA512
from Crypto.Cipher import AES
from hashlib import pbkdf2_hmac as secretkey_spec

key = b'test pass'
iv = b'nonce'
cipher = AES.new(key, AES.MODE_CTR, nonce=iv)
