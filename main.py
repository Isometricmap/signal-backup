import backup_pb2
from Crypto.Hash import HMAC, SHA256, SHA512
from Crypto.Cipher import AES
from hashlib import pbkdf2_hmac as secretkey_spec
import math
import signal_backup

test_pass = ''

backup_db = backup_pb2.BackupFrame()

f = open("test.backup", 'rb')
f.seek(4)
backup_db.ParseFromString(f.read(54))
iv = backup_db.header.iv[:-1]
salt = backup_db.header.salt

cipher = signal_backup.DecryptionStream(iv, salt, test_pass)

f.seek(62)
buffer = f.read(14)
backup_db = backup_pb2.BackupFrame()
backup_db.ParseFromString(cipher.decrypt(buffer))

print(backup_db.ListFields())
#
# backup_db.ParseFromString(f.read(14))
# print(backup_db.ListFields())
