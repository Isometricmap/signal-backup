import backup_pb2
from Crypto.Hash import HMAC, SHA256, SHA512
from Crypto.Cipher import AES
from hashlib import pbkdf2_hmac as secretkey_spec
import math

HASH_OUTPUT_SIZE = 32


class DecryptionStream:
    digest_512 = SHA512.new()

    def __init__(self, iv, salt, passphrase):
        self.iv = iv
        self.salt = salt
        key = self.gen_key(passphrase)
        derived = HDKF(key, b"Backup Export", 64)

        self.cipherkey = derived[:32]
        self.mackey = derived[32::]

        self.cipher = AES.new(self.cipherkey, AES.MODE_CTR, nonce=self.iv)
        self.mac = HMAC.new(self.mackey, msg=None, digestmod=SHA256)
        self.counter = int.from_bytes(self.iv, byteorder='big')

    def gen_key(self, passphrase):
        digest_input = passphrase.replace(" ", "").encode('utf-8')
        hash = digest_input

        if (self.salt != None):
            self.digest_512.update(hash)

        for i in range(250000):
            self.digest_512.update(hash)
            self.digest_512.update(digest_input)
            hash = self.digest_512.digest()

        return hash[:32]

    def decrypt(self, buffer):
        output = self.cipher.decrypt(buffer)
        return output


def HDKF(input_key, info, output_length):
    salt = b''
    return derive_secrets(input_key, salt, info, output_length)


def derive_secrets(input_key, salt, info, output_length):
    prk = extract(salt, input_key)
    return expand(prk, info, output_length)


def extract(salt, input_key):
    mac = HMAC.new(salt, msg=None, digestmod=SHA256)
    mac.update(input_key)
    return mac.digest()


def expand(prk, info, output_length):
    iterations = int(math.ceil(output_length / HASH_OUTPUT_SIZE))
    remaining_bytes = output_length
    mixin = b''
    output = b''

    for i in range(1, iterations + 1):
        mac = HMAC.new(prk, msg=None, digestmod=SHA256)
        mac.update(mixin)

        if info != None:
            mac.update(info)
        mac.update(bytes([i]))

        mac_digest = mac.digest()
        step_size = min(remaining_bytes, len(mac_digest))

        output += mac_digest

        mixin = mac_digest
        remaining_bytes -= step_size

    return output
